import React from 'react'
import '../App.css';

function componentName() {
    return (
        <div className="center">
            <h1 className="changecolor">
                I Love Programming!
            </h1>
        </div>
    )
}

export default componentName
