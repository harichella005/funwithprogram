import React from 'react'

function componentName() {
    return (
        <div className="box">
            <h3 className="text">Coder</h3>
        </div>
    )
}

export default componentName
